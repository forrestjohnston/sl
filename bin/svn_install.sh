#!/bin/bash

install_to=c:/usrbin
downloads=C:/Users/forrest/Downloads
# installer=Setup-Subversion-1.6.16.msi
installer=svn-win32-1.6.16.zip

echo Downloading Subversion Client ...
cd $downloads
# wget http://sourceforge.net/projects/win32svn/files/1.6.16/Setup-Subversion-1.6.16.msi
wget http://sourceforge.net/projects/win32svn/files/1.6.16/svn-win32-1.6.16.zip

echo Expanding $installer ...
unzip -x -o -q $installer -d $install_to 

echo Renaming to $install_to/svn16
mv $install_to/svn-win32-1.6.16 $install_to/svn16

if [ ! -f $downloads/$installer ]; then
    echo "ERROR file not found: $installer"
    exit 1
fi

mysvn=`which svn`
if [ ! -f "$mysvn" ]; then
    echo Append PATH ... c:/usrbin/svn16/bin
    setx PATH "%PATH%;c:/usrbin/svn16/bin;"
fi
mysvn=`which svn`
echo which svn: $mysvn

echo complete.
sleep 15

