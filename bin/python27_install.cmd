@echo off
set software="\\SLQA46\Software"
set downloads="%USERPROFILE%\Downloads"
set pyinstall="python-2.7.8.amd64.msi"
set installer="%downloads%\%pyinstall%"

echo "Copy (or download) Python 2.7 ..."
copy "%software%\%pyinstall%" $downloads

if not exist %installer% (
    echo "ERROR file not found: %installer%"
    goto :eof
)

cd %downloads%
call start cmd.exe /c %installer%
call sleep 3
echo Start Sikuli 
call C:\usrbin\sikuli\runsikulix.cmd -r C:\sl\bin\sikuli\python27_install.sikuli > %LOGS%\python27_install.sikuli.log.txt 2>&1
echo "Append C:\usrbin\python27 to PATH ..."
setx PATH "%PATH%;C:\usrbin\python27;"
:eof