#!/bin/bash

downloads=C:/Users/forrest/Downloads
gitexe="C:/usrbin/git/bin/git.exe"
gitfile=Git-1.9.5-preview20141217.exe
gitusr="\"Forrestjohnston\""
gitemail="\"forrest\@forrestjohnston.com\""

echo Downloading Git ...
cd $downloads
wget https://github.com/msysgit/msysgit/releases/download/Git-1.9.5-preview20141217/$gitfile

if [ ! -f $downloads/$gitfile ]; then
    echo "ERROR file not found: $gitfile"
    exit 1
fi

./$gitfile

if [ ! -f $gitexe ]; then
    echo "ERROR file not found: $gitexe"
    exit 1
fi

echo Git Install Complete
echo Setup gitusr=$gitusr gitemail=$gitemail
$gitexe config --global user.name $gitusr
$gitexe config --global user.email $gitemail

cd c:/cygwin64/tmp
$gitexe clone https://github.com/Forrestjohnston/sl.git 
cd sl
mv .git c:/sl
cd c:/cygwin64/tmp
echo "TODO: Cleanup..."
# rm -rf sl
cd c:/sl
echo "TODO: rsync -av source dest ..."
echo complete.
sleep 15

