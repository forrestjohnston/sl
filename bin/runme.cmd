@echo off
set LOGS=C:\sl\logs
mkdir %LOGS% > %TEMP%\nul 2>&1

echo runme.cmd is running...
echo %USERNAME% logged on at %DATE% %TIME% >> %LOGS%\runme_log.txt

echo #################
echo Provision Here...
echo #################
::echo Start Sikuli 
::call C:\usrbin\sikuli\runsikulix.cmd -r C:\sl\sikuli\git.sikuli > %LOGS%\git.sikuli.log.txt 2>&1
